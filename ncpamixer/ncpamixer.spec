Name:       ncpamixer
Version:    1.3.3.1
Release:    1%{?dist}
Summary:    A Ncurses mixer for PulseAudio inspired by pavucontrol

License:    MIT
URL:        https://github.com/fulhax/ncpamixer
Source0:    %{url}/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(menu)
BuildRequires:  pkgconfig(ncursesw)

%description
Ncurses-based PulseAudio mixer and controller.

%prep
%autosetup -p1

%build
%{set_build_flags}
export CFLAGS="${CFLAGS} -Wno-error=stringop-truncation"
export CXXFLAGS="${CFLAGS} -Wno-error=stringop-truncation"

%{cmake} -DCMAKE_BUILD_TYPE=RelWithDebInfo src
%{cmake_build}

%install
%{cmake_install}


%files
%{_bindir}/%{name}

%changelog
* Mon Nov 16 2020 Jan “Khardix” Staněk <khardix@gmail.com> - 1.3.3.1-1
- Upgrade to version 1.3.3.1
- Fix CMake build instructions

* Wed Apr 01 2020 Jan Staněk <jstanek@redhat.com> - 1.3.3-2
- Disable -Werror on distro builds

* Wed Nov 06 2019 Jan Staněk <jstanek@redhat.com> - 1.3.3-1
- Upgrade to version 1.3.3

* Mon Oct 01 2018 Jan Staněk <jstanek@redhat.com> - 1.2-2
- Add gcc-c++ to BuildRequires for F29+

* Tue Apr 24 2018 Jan Staněk <jstanek@redhat.com> - 1.2-1
- Initial package import from github.
