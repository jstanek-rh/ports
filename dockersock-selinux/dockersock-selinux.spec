%global     git_commit      240caff55333754dd782ce694ef4f54d840bc15c
%global     git_date        20190724
%global     git_shortcommit %(c=%{git_commit}; echo ${c:0:7})

%global     selinuxtype     targeted
%global     selinuxpkgdir   %{_datadir}/selinux/packages/%{selinuxtype}
%global     moduletype      contrib
%global     modulename      dockersock

Name:       %{modulename}-selinux
Version:    0
Release:    1.%{git_date}git%{git_shortcommit}%{?dist}
Summary:    Allow containers to access docker.sock under Fedora and RHEL

License:    Apache-2.0
URL:        https://github.com/dpw/selinux-dockersock
Source0:    %{url}/archive/%{git_commit}/%{name}-%{git_shortcommit}.tar.gz

BuildArch:  noarch
BuildRequires:  selinux-policy selinux-policy-devel
%{?selinux_requires}

%description
Small SELinux module that allows containers to access the docker.sock socket.

%prep
%autosetup -p1 -n selinux-dockersock-%{git_commit}

%build
make %{modulename}.pp && bzip2 -9 %{modulename}.pp

%pre
%selinux_relabel_pre -s %{selinuxtype}

%install
install -m0644 -Dt %{buildroot}%{selinuxpkgdir} %{modulename}.pp.bz2

%check

%post
%selinux_modules_install -s %{selinuxtype} %{selinuxpkgdir}/%{modulename}.pp.bz2

%postun
if [ $1 -eq 0 ]; then
    %selinux_modules_uninstall -s %{selinuxtype} %{modulename}
fi

%posttrans
%selinux_relabel_post -s %{selinuxtype}

%files
%doc README.md
%license LICENSE
%{selinuxpkgdir}/%{modulename}.pp.bz2
%ghost %{_sharedstatedir}/selinux/%{selinuxtype}/active/modules/200/%{modulename}


%changelog
* Wed Jul 24 2019 Jan Staněk <jstanek@redhat.com> - 0.1.20190724git240caff
- Initial package
