Name:       i3lock-color
Version:    2.12.c
Release:    1%{?dist}
Summary:    Improved improved screen locker with color configuration

License:    MIT
URL:        https://github.com/PandorasFox/i3lock-color
Source0:    https://github.com/PandorasFox/%{name}/archive/%{version}.tar.gz

BuildRequires: gcc
BuildRequires: autoconf automake
BuildRequires: libev-devel
BuildRequires: pam-devel
BuildRequires: pkgconfig(cairo)
BuildRequires: pkgconfig(fontconfig)
BuildRequires: pkgconfig(libjpeg) >= 1.4.90
BuildRequires: pkgconfig(xcb)
BuildRequires: pkgconfig(xcb-composite)
BuildRequires: pkgconfig(xcb-image)
BuildRequires: pkgconfig(xcb-randr)
BuildRequires: pkgconfig(xcb-util)
BuildRequires: pkgconfig(xcb-xrm)
BuildRequires: pkgconfig(xkbcommon) >= 0.5.0
BuildRequires: pkgconfig(xkbcommon-x11) >= 0.5.0

Provides:   i3lock = %{version}
Conflicts:  i3lock

%description
i3lock is a simple screen locker like slock. After starting it, you will see a
white screen (you can configure the color/an image). You can return to your
screen by entering your password.

Additional features in this fork:
- Color options
- Blurring the current screen and using that as the lock background
- Showing a clock in the indicator
- refreshing on a timer, instead of on each key press
- Positioning the various UI elements
- Changing the ring radius and thickness, as well as text size
- A new bar indicator, which replaces the ring indicator
  with its own set of options

%prep
%setup -q
autoreconf -i

%build
%configure --enable-debug=info
make %{?_smp_mflags}

%install
%make_install

%files
%{_bindir}/i3lock
%config(noreplace) /etc/pam.d/i3lock
%{_mandir}/man1/i3lock.1.gz

%changelog
* Thu Oct 25 2018 Jan Staněk <jstanek@redhat.com> - 2.12.c-1
- Update to 2.12.c (https://github.com/PandorasFox/i3lock-color/releases/tag/2.12.c)

* Mon Oct 01 2018 Jan Staněk <jstanek@redhat.com> - 2.11-2
- Add gcc to BuildRequires for F29+

* Wed Apr 18 2018 Jan Staněk <jstanek@redhat.com> - 2.11-1
- Import package from github
