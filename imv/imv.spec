Name:       imv
Version:    4.0.1
Release:    1%{?dist}
Summary:    Command line image viewer for tiling window managers

License:    MIT
URL:        https://github.com/eXeC64/imv
Source0:    %{url}/archive/v%{version}/%{name}-v%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  asciidoc
BuildRequires:  desktop-file-utils
# Required always
BuildRequires:  pkgconfig(icu-io)
BuildRequires:  pkgconfig(pangocairo)
BuildRequires:  pkgconfig(xkbcommon)
# Fox X11
BuildRequires:  pkgconfig(gl)
BuildRequires:  pkgconfig(glu)
BuildRequires:  pkgconfig(x11)
BuildRequires:  pkgconfig(xcb)
BuildRequires:  pkgconfig(xkbcommon-x11)
# For wayland
BuildRequires:  pkgconfig(egl)
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-egl)
# For default backends
BuildRequires:  freeimage-devel
BuildRequires:  pkgconfig(librsvg-2.0)
# For tests
BuildRequires:  pkgconfig(cmocka)

%description
%{name} is a command line image viewer
intended for use with tiling window managers.

%prep
%autosetup -p1

%build
%set_build_flags
make %{?_smp_mflags}


%install
%make_install DESTDIR=%{buildroot}
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop


%check
make %{?_smp_mflags} check


%files
%doc README.md CHANGELOG
%license LICENSE
%{_bindir}/%{name}{,-msg,-wayland,-x11}
%{_mandir}/man{1,5}/%{name}{,-msg}.*
%{_datadir}/applications/%{name}.desktop
%config %{_sysconfdir}/%{name}_config


%changelog
* Wed Nov 06 2019 Jan Staněk <jstanek@redhat.com> - 4.0.1-1
- Upgrade to version 4.0.1

* Fri Jul 26 2019 Jan Staněk <jstanek@redhat.com> - 3.1.2-1
- Initial package
